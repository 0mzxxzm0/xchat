package xchat.access.web;

import xchat.access.AccessResponse;
import xchat.access.AccessStatus;

import javax.servlet.http.HttpSession;

public class Validator {
    public static void validateName(AccessResponse accessResponse, String name) {
        if (name.length() == 0 || name.length() > 10) {
            accessResponse.setStatus(AccessStatus.ERROR);
            accessResponse.setInfo("Invalid name, the length of the name must be in [1, 10]!");
        }
    }

    public static void validatePassword(AccessResponse accessResponse, String password) {
        if (password.length() == 0 || password.length() > 10) {
            accessResponse.setStatus(AccessStatus.ERROR);
            accessResponse.setInfo("Invalid password, the length of the password must be in [1, 10]!");
        }
    }

    public static void checkLogin(AccessResponse accessResponse, HttpSession session) {
        if (session.getAttribute("userInfo") == null) {
            accessResponse.setStatus(AccessStatus.ERROR);
            accessResponse.setInfo("You haven't login.");
        }
    }
}
