package xchat.access.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class HandshakeInterceptor extends HttpSessionHandshakeInterceptor {
    private final Logger log = LoggerFactory.getLogger(HandshakeInterceptor.class);
    // Allowed WebSocket parameter names (eg : "ws://localhost/websocket?name=m&password=p")
    private final List<String> webSocketParameterNames = Arrays.asList("name", "password");

    @Override
    public boolean beforeHandshake(
            ServerHttpRequest request,
            ServerHttpResponse response,
            WebSocketHandler wsHandler,
            Map<String, Object> attributes) throws Exception {
        log.info("Inject WebSocket parameters to WebSocketSession.");
        HttpServletRequest req = ((ServletServerHttpRequest)request).getServletRequest();
        for (String paraName : webSocketParameterNames) {
            attributes.put(paraName, req.getParameter(paraName));
        }
        return super.beforeHandshake(request, response, wsHandler, attributes);
    }
}
