package xchat.access.web;

import xchat.data.model.User;

public class FilteredUser {
    private int uid;
    private String name;

    public FilteredUser(User user) {
        this.uid = user.getUid();
        this.name = user.getName();
    }

    public int getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }
}
