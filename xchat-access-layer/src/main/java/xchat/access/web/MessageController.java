package xchat.access.web;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import xchat.access.AccessResponse;
import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessRequestType;
import xchat.business.common.BusinessService;
import xchat.data.model.Message;
import xchat.data.model.User;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
public class MessageController {
    @Reference
    private BusinessService service;
    @Autowired
    private MessageFilter messageFilter;

    @RequestMapping(value = "/action/get_conversation", method = RequestMethod.GET)
    public @ResponseBody AccessResponse getConversation(
            @RequestParam("friend_uid") int friend_uid,
            @RequestParam("time") long time,
            @RequestParam("limit") int limit,
            HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.checkLogin(accessResponse, session);
        if (!accessResponse.ok()) return accessResponse;
        // TODO : check parameters !!!
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.MESSAGE, "get_conversation");
        businessRequest.addParameter("my_uid", ((User)session.getAttribute("userInfo")).getUid());
        businessRequest.addParameter("friend_uid", friend_uid);
        businessRequest.addParameter("time", time);
        businessRequest.addParameter("limit", limit);
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        if (accessResponse.ok()) {
            accessResponse.setResult(messageFilter.filterAll((List<Message>) accessResponse.getResult()));
        }
        // TODO : parameter (start, count)!!!
        return accessResponse;
    }

    @RequestMapping(value = "/action/get_unread_messages", method = RequestMethod.GET)
    public @ResponseBody AccessResponse getConversation(HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.checkLogin(accessResponse, session);
        if (!accessResponse.ok()) return accessResponse;
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.MESSAGE, "get_unread_messages");
        businessRequest.addParameter("uid", ((User)session.getAttribute("userInfo")).getUid());
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        if (accessResponse.ok()) {
            accessResponse.setResult(messageFilter.filterAll((List<Message>) accessResponse.getResult()));
        }
        return accessResponse;
    }
}
