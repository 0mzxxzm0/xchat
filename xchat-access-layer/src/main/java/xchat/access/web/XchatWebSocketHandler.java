package xchat.access.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;
import xchat.access.AccessServer;
import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessRequestType;
import xchat.business.common.BusinessResponse;
import xchat.business.common.BusinessService;
import xchat.data.model.*;

import java.util.HashMap;

public class XchatWebSocketHandler extends AbstractWebSocketHandler {
    private final Logger log = LoggerFactory.getLogger(XchatWebSocketHandler.class);
    private ObjectMapper jsonMapper = new ObjectMapper();

    @Reference
    private BusinessService service;

    @Autowired
    private AccessServer server;
    @Autowired
    private MessageFilter messageFilter;

    private HashMap<Integer, WebSocketSession> sessionHashMap = new HashMap<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String name = (String) session.getAttributes().get("name");
        String password = (String) session.getAttributes().get("password");
        if (name == null || password == null) {
            // TODO : session.send(errorMessage);
            log.error("error sending message.");
            session.close();
        }
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.WEBSOCKET, "open");
        businessRequest.addParameter("userInfo", new User(name, password));
        businessRequest.addParameter("endpoint", server.getServerId());
        BusinessResponse businessResponse = service.handleBusinessRequest(businessRequest);
        if (!businessResponse.ok()) {
            // TODO : error message
            log.error("login error.");
            session.close();
            return;
        }
        User user = (User) businessResponse.getResult();
        session.getAttributes().put("uid", user.getUid());
        sessionHashMap.put(user.getUid(), session);
        log.info("WebSocket connection established.");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        if (session.getAttributes().get("uid") != null) {
            int uid = (int) session.getAttributes().get("uid");
            BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.WEBSOCKET, "close");
            businessRequest.addParameter("uid", uid);
            BusinessResponse businessResponse = service.handleBusinessRequest(businessRequest);
            if (!businessResponse.ok()) {
                // TODO : error closing webSocket
                log.error("error closing webSocket.");
            }
            sessionHashMap.remove(uid);
        }
        log.info("WebSocket connection closed");
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage jsonMessage) throws Exception {
        log.info("WebSocket received message : " + jsonMessage.getPayload());
        ClientMessage clientMessage = jsonMapper.readValue(jsonMessage.getPayload(), ClientMessage.class);
        int uid = (int) session.getAttributes().get("uid");
        Message message = new Message(uid, clientMessage.getTo());
        message.setType(MessageType.NORMAL);
        message.setStatus(MessageStatus.SENT);
        message.setContent(clientMessage.getContent());
        long startTime = System.currentTimeMillis();
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.MESSAGE, "send");
        businessRequest.addParameter("message", message);
        BusinessResponse businessResponse = service.handleBusinessRequest(businessRequest);
        log.info("handle message time : " + (System.currentTimeMillis() - startTime));
        if (!businessResponse.ok()) {
            // TODO : fail to send message
            log.error(businessResponse.getInfo());
        }
    }

    public void pushMessage(Message message) {
        try {
            String jsonMessage = jsonMapper.writeValueAsString(messageFilter.filter(message));
            int uid = message.getDirection() == MessageDirection.BIG2SMALL
                    ? message.getSmallUid() : message.getBigUid();
            sessionHashMap.get(uid).sendMessage(new TextMessage(jsonMessage));
        } catch (Exception e) {
            // TODO : error push message
            log.error("error pushing message.");
            e.printStackTrace();
        }
    }
}

class ClientMessage {
    private int to;
    private String content;

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}