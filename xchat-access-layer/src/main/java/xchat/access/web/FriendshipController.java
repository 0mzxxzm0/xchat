package xchat.access.web;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import xchat.access.AccessResponse;
import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessRequestType;
import xchat.business.common.BusinessService;
import xchat.data.model.Message;
import xchat.data.model.User;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FriendshipController {
    @Reference
    private BusinessService service;
    @Autowired
    private MessageFilter messageFilter;

    // TODO : user filter to do login status check ?
    @RequestMapping(value = "/action/apply_friendship", method = RequestMethod.GET)
    public @ResponseBody AccessResponse applyFriendship(@RequestParam("uid") int to, HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.checkLogin(accessResponse, session);
        if (!accessResponse.ok()) return accessResponse;
        // TODO : check parameter [uid]?
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.FRIENDSHIP, "apply");
        businessRequest.addParameter("from", ((User)session.getAttribute("userInfo")).getUid());
        businessRequest.addParameter("to", to);
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        return accessResponse;
    }

    @RequestMapping(value = "/action/approve_friendship", method = RequestMethod.GET)
    public @ResponseBody AccessResponse searchUser(@RequestParam("mid") int mid, HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.checkLogin(accessResponse, session);
        if (!accessResponse.ok()) return accessResponse;
        // TODO : check parameter [mid]?
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.FRIENDSHIP, "approve");
        businessRequest.addParameter("uid", ((User)session.getAttribute("userInfo")).getUid());
        businessRequest.addParameter("mid", mid);
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        return accessResponse;
    }


    @RequestMapping(value = "/action/get_friends", method = RequestMethod.GET)
    public @ResponseBody AccessResponse getFriends(HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.checkLogin(accessResponse, session);
        if (!accessResponse.ok()) return accessResponse;
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.FRIENDSHIP, "get_friends");
        businessRequest.addParameter("uid", ((User)session.getAttribute("userInfo")).getUid());
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        List<FilteredUser> filteredUserList = new ArrayList<>();
        List<User> users = (List<User>) accessResponse.getResult();
        for (User user : users) {
            filteredUserList.add(new FilteredUser(user));
        }
        accessResponse.setResult(filteredUserList);
        return accessResponse;
    }

    @RequestMapping(value = "/action/get_friendship_applications", method = RequestMethod.GET)
    public @ResponseBody AccessResponse getFriendshipApplications(HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.checkLogin(accessResponse, session);
        if (!accessResponse.ok()) return accessResponse;
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.FRIENDSHIP, "get_friendship_applications");
        businessRequest.addParameter("uid", ((User)session.getAttribute("userInfo")).getUid());
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        accessResponse.setResult(messageFilter.filterAll((List<Message>)accessResponse.getResult()));
        return accessResponse;
    }
}
