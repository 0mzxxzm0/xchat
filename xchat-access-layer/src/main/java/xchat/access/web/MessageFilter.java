package xchat.access.web;


import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;
import xchat.data.model.Message;
import xchat.data.model.MessageDirection;
import xchat.service.user.common.UserService;

import java.util.ArrayList;
import java.util.List;

@Component
public class MessageFilter {

    FilteredMessage filter(Message message) {
        FilteredMessage filteredMessage = new FilteredMessage();
        filteredMessage.setMid(message.getMid());
        filteredMessage.setType(message.getType());
        filteredMessage.setStatus(message.getStatus());
        filteredMessage.setContent(message.getContent());
        filteredMessage.setTime(message.getTime());
        if (message.getDirection() == MessageDirection.BIG2SMALL) {
            filteredMessage.setFrom(message.getBigUid());
            filteredMessage.setTo(message.getSmallUid());
        } else {
            filteredMessage.setFrom(message.getSmallUid());
            filteredMessage.setTo(message.getBigUid());
        }
        return filteredMessage;
    }

    List<FilteredMessage> filterAll(List<Message> messageList) {
        List<FilteredMessage> filteredMessages = new ArrayList<>();
        for (Message message : messageList) {
            filteredMessages.add(filter(message));
        }
        return filteredMessages;
    }
}
