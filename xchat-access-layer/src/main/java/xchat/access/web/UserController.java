package xchat.access.web;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import xchat.access.AccessResponse;
import xchat.access.AccessStatus;
import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessRequestType;
import xchat.business.common.BusinessService;
import xchat.data.model.User;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {
    @Reference
    private BusinessService service;

    // TODO : do login status checking using Spring Security of AOP ??
    @RequestMapping(value = "/action/register", method = RequestMethod.POST)
    public @ResponseBody AccessResponse register(User user) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.validateName(accessResponse, user.getName());
        if (!accessResponse.ok()) return accessResponse;
        Validator.validatePassword(accessResponse, user.getPassword());
        if (!accessResponse.ok()) return accessResponse;
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.USER, "register");
        businessRequest.addParameter("userInfo", user);
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        return accessResponse;
    }

    @RequestMapping(value = "/action/login", method = RequestMethod.POST)
    public @ResponseBody AccessResponse login(User user, HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        if (session.getAttribute("userInfo") != null) {
            accessResponse.setStatus(AccessStatus.ERROR);
            accessResponse.setInfo("you already logged in!");
            return accessResponse;
        }
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.USER, "login");
        businessRequest.addParameter("userInfo", user);
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        if (accessResponse.ok()) {
            int uid = (int)accessResponse.getResult();
            user.setUid(uid);
            session.setAttribute("userInfo", user);
            session.setAttribute("loginTime", System.currentTimeMillis());
        }
        return accessResponse;
    }

    @RequestMapping(value = "/action/search_user_by_name", method = RequestMethod.GET)
    public @ResponseBody AccessResponse searchUser(@RequestParam("name") String name, HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.checkLogin(accessResponse, session);
        if (!accessResponse.ok())
            return accessResponse;
        Validator.validateName(accessResponse, name);
        if (!accessResponse.ok())
            return accessResponse;
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.USER, "search_by_name");
        businessRequest.addParameter("name", name);
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        if (accessResponse.ok())
            accessResponse.setResult(new FilteredUser((User) accessResponse.getResult()));
        return accessResponse;
    }

    @RequestMapping(value = "/action/search_user_by_id", method = RequestMethod.GET)
    public @ResponseBody AccessResponse searchUser(@RequestParam("uid") int uid, HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.checkLogin(accessResponse, session);
        if (!accessResponse.ok())
            return accessResponse;
        BusinessRequest businessRequest = new BusinessRequest(BusinessRequestType.USER, "search_by_id");
        businessRequest.addParameter("uid", uid);
        accessResponse.handleBusinessResponse(service.handleBusinessRequest(businessRequest));
        if (accessResponse.ok())
            accessResponse.setResult(new FilteredUser((User) accessResponse.getResult()));
        return accessResponse;
    }

    @RequestMapping(value = "/action/my_info", method = RequestMethod.GET)
    public @ResponseBody AccessResponse getMyInfo(HttpSession session) {
        AccessResponse accessResponse = new AccessResponse();
        Validator.checkLogin(accessResponse, session);
        if (!accessResponse.ok())
            return accessResponse;
        Map<String, Object> res = new HashMap<>();
        res.put("user_info", session.getAttribute("userInfo"));
        res.put("login_time", session.getAttribute("loginTime"));
        accessResponse.setResult(res);
        return accessResponse;
    }

    @RequestMapping(value = "/action/logout", method = RequestMethod.GET)
    public @ResponseBody AccessResponse logout(HttpSession session) {
        session.removeAttribute("userInfo");
        session.removeAttribute("loginTime");
        AccessResponse response = new AccessResponse();
        response.setStatus(AccessStatus.OK);
        response.setInfo("");
        return response;
    }
}
