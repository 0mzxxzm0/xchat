package xchat.access.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Reference;
import xchat.data.model.Message;
import xchat.data.model.MessageDirection;
import xchat.data.model.MessageStatus;
import xchat.data.model.MessageType;
import xchat.service.user.common.UserService;

import java.util.Date;

public class FilteredMessage {
    private int mid;
    private int from;
    private int to;
    private MessageType type;
    private MessageStatus status;
    private long time;
    private String timeString;
    private String content;

    public FilteredMessage() {}

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public MessageStatus getStatus() {
        return status;
    }

    public void setStatus(MessageStatus status) {
        this.status = status;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @JsonProperty("time_string")
    public String getTimeString() {
        return timeString;
    }

    public void setTimeString(String timeString) {
        this.timeString = timeString;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
