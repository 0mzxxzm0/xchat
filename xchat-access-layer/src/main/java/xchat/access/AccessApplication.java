package xchat.access;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import xchat.access.web.XchatWebSocketHandler;
import xchat.data.model.Message;

import java.io.IOException;

@SpringBootApplication
@DubboComponentScan("xchat.access")
public class AccessApplication implements CommandLineRunner {
    @Autowired
    private AccessServer server;
    @Autowired
    private XchatWebSocketHandler webSocketHandler;
    private ObjectMapper jsonMapper = new ObjectMapper();

    public static void main(String[] args) {
        SpringApplication.run(AccessApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(server.getServerId(), false, false, false, null);
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String jsonMessage = new String(body, "UTF-8");
                Message message = jsonMapper.readValue(jsonMessage, Message.class);
                webSocketHandler.pushMessage(message);
            }
        };
        channel.basicConsume(server.getServerId(), true, consumer);
    }
}
