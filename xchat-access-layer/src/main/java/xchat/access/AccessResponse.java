package xchat.access;

import xchat.business.common.BusinessResponse;

public class AccessResponse {
    private AccessStatus status;
    private String info;
    private Object result;

    public AccessResponse() {
        this.status = AccessStatus.OK;
        this.info = "";
        this.result = null;
    }

    public AccessStatus getStatus() {
        return status;
    }

    public void setStatus(AccessStatus status) {
        this.status = status;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public void handleBusinessResponse(BusinessResponse businessResponse) {
        switch (businessResponse.getStatus()) {
            case OK:
                this.status = AccessStatus.OK;
                break;
            case ERROR:
                this.status = AccessStatus.ERROR;
                break;
        }
        this.info = businessResponse.getInfo();
        this.result = businessResponse.getResult();
    }

    public boolean ok() {
        return this.status == AccessStatus.OK;
    }
}
