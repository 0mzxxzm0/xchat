package xchat.access;

import org.springframework.stereotype.Component;

@Component
public class AccessServer {
    // TODO : Config serverId using *.properties
    private String serverId = "access_server";

    public String getServerId() {
        return serverId;
    }
}
