package xchat.service.messagepush.config;

import com.rabbitmq.client.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqConfig {
    @Bean
    public ConnectionFactory connectionFactory() {
        // TODO : use *.properties to config rabbitmq.
        ConnectionFactory cf = new ConnectionFactory();
        cf.setHost("localhost");
        return cf;
    }
}
