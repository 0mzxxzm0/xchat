package xchat.service.messagepush.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import xchat.data.model.Message;
import xchat.data.model.MessageDirection;
import xchat.service.messagepush.common.MessagePushService;

@Service(timeout = 3000, retries = -1)
public class MessagePushServiceImpl implements MessagePushService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private ConnectionFactory connectionFactory;
    private ObjectMapper jsonMapper = new ObjectMapper();
    private Logger log = LoggerFactory.getLogger(MessagePushServiceImpl.class);

    @Override
    public boolean registerEndpoint(int uid, String endpoint) {
        redisTemplate.opsForValue().set(uid, endpoint);
        return redisTemplate.opsForValue().get(uid) != null;
    }

    @Override
    public boolean unRegisterEndpoint(int uid) {
        redisTemplate.delete(uid);
        return redisTemplate.opsForValue().get(uid) == null;
    }

    @Override
    public boolean pushMessage(Message message) {
        int to = message.getDirection() == MessageDirection.BIG2SMALL ?
                message.getSmallUid() : message.getBigUid();
        if (redisTemplate.opsForValue().get(to) == null) {
            log.error("Fail to look up endpoint!");
            return false;
        }
        String endpoint = (String) redisTemplate.opsForValue().get(to);
        try {
            String textMessage = jsonMapper.writeValueAsString(message);
            // TODO : don't need to create a connection for every message???
            Connection connection = connectionFactory.newConnection();
            Channel channel = connection.createChannel();
            channel.basicPublish("", endpoint, null, textMessage.getBytes());
            channel.close();
            connection.close();
        } catch (Exception e) {
            log.error("Exception happened whiling publish message to rabbitmq!(ERROR:" + e + ")");
            return false;
        }
        return true;
    }
}
