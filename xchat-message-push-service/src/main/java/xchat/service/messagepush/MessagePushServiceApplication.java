package xchat.service.messagepush;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@DubboComponentScan(basePackages = {"xchat.service.messagepush.impl"})
public class MessagePushServiceApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(MessagePushServiceApplication.class, args);
        System.in.read();
    }
}
