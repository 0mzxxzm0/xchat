package xchat.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Message implements Serializable {
    private int mid;
    private int bigUid;
    private int smallUid;
    private MessageDirection direction;
    private long time;
    private MessageType type;
    private MessageStatus status;
    private String content;

    Message() {}

    public Message(int from, int to) {
        if (from > to) {
            this.bigUid = from;
            this.smallUid = to;
            this.direction = MessageDirection.BIG2SMALL;
        } else {
            this.bigUid = to;
            this.smallUid = from;
            this.direction = MessageDirection.SMALL2BIG;
        }
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    @JsonProperty("big_uid")
    public int getBigUid() {
        return bigUid;
    }

    public void setBigUid(int bigUid) {
        this.bigUid = bigUid;
    }

    @JsonProperty("small_uid")
    public int getSmallUid() {
        return smallUid;
    }

    public void setSmallUid(int smallUid) {
        this.smallUid = smallUid;
    }

    public MessageDirection getDirection() {
        return direction;
    }

    public void setDirection(MessageDirection direction) {
        this.direction = direction;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public MessageStatus getStatus() {
        return status;
    }

    public void setStatus(MessageStatus status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
