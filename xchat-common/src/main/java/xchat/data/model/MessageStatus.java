package xchat.data.model;

public enum MessageStatus {
    SENT, READ
}
