package xchat.data.model;

public enum MessageDirection {
    BIG2SMALL, SMALL2BIG
}
