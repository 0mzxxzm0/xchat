package xchat.data.model;

import java.io.Serializable;

public class User implements Serializable {
    private int uid;
    private String name;
    private String password;

    public User() {}

    public User(String name, String password) {
        this.uid = -1;
        this.name = name;
        this.password = password;
    }

    public User(int id, String name, String password) {
        this.uid = id;
        this.name = name;
        this.password = password;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int id) {
        this.uid = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
