package xchat.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Friendship implements Serializable {
    private int fid;
    private int bigUid;
    private int smallUid;

    public Friendship() {}

    public Friendship(int user1, int user2) {
        if (user1 > user2) {
            bigUid = user1;
            smallUid = user2;
        } else {
            bigUid = user2;
            smallUid = user1;
        }
    }

    public int getFid() {
        return fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    @JsonProperty("big_uid")
    public int getBigUid() {
        return bigUid;
    }

    public void setBigUid(int bigUid) {
        this.bigUid = bigUid;
    }

    @JsonProperty("small_uid")
    public int getSmallUid() {
        return smallUid;
    }

    public void setSmallUid(int smallUid) {
        this.smallUid = smallUid;
    }
}
