package xchat.service.messagepush.common;

import xchat.data.model.Message;

public interface MessagePushService {
    boolean registerEndpoint(int uid, String endpoint);
    boolean unRegisterEndpoint(int uid);
    // TODO : multi thread
    boolean pushMessage(Message message);
}
