package xchat.service.messagestore.common;

import xchat.data.model.Friendship;
import xchat.data.model.Message;
import xchat.data.model.MessageStatus;

import java.util.List;

public interface MessageStoreService {
    int storeMessage(Message message);
    int deleteMessage(int mid);
    Message getMessageById(int mid);
    List<Message> getConversation(Friendship f, long time, int count);
    List<Message> getFriendshipApplications(int uid);
    Message getFriendshipApplication(int from, int to);
    int readMessage(int mid);
    List<Message> getUnreadMessages(int uid);
}
