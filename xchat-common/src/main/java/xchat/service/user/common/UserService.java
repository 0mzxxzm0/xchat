package xchat.service.user.common;

import xchat.data.model.Friendship;
import xchat.data.model.User;

import java.util.List;

public interface UserService {
    int register(User user);
    int login(User user);
    User searchUserByName(String name);
    User searchUserById(int uid);
    boolean isFriends(Friendship f);
    List<User> getFriends(int uid);
    int createFriendship(Friendship friendship);
}
