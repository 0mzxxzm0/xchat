package xchat.business.common;

import java.io.Serializable;

public class BusinessResponse implements Serializable {
    private BusinessResponseStatus status;
    private String info;
    private Object result;

    public BusinessResponse(BusinessResponseStatus status, String info, Object result) {
        this.status = status;
        this.info = info;
        this.result = result;
    }

    public BusinessResponseStatus getStatus() {
        return status;
    }

    public Object getResult() {
        return result;
    }

    public String getInfo() {
        return info;
    }

    public void setStatus(BusinessResponseStatus status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public boolean ok() {
        return this.status == BusinessResponseStatus.OK;
    }
}
