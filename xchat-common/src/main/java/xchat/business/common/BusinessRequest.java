package xchat.business.common;

import java.io.Serializable;
import java.util.HashMap;

public class BusinessRequest implements Serializable {
    private BusinessRequestType type;
    private String command;
    private HashMap<String, Object> parameters;

    public BusinessRequest(BusinessRequestType type, String command) {
        this.type = type;
        this.command = command;
        parameters = new HashMap<String, Object>();
    }

    public BusinessRequestType getType() {
        return type;
    }

    public String getCommand() {
        return command;
    }

    public void addParameter(String key, Object value) {
        parameters.put(key, value);
    }

    public Object getParameter(String key) {
        return parameters.get(key);
    }

    public HashMap<String, Object> getParameters() {
        return parameters;
    }
}
