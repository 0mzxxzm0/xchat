package xchat.business.common;

public interface BusinessService {
    BusinessResponse handleBusinessRequest(BusinessRequest request);
}
