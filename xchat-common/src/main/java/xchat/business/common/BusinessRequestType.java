package xchat.business.common;

public enum BusinessRequestType {
    USER, FRIENDSHIP, MESSAGE, WEBSOCKET
}