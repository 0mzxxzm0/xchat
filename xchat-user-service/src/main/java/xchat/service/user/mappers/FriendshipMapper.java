package xchat.service.user.mappers;

import xchat.data.model.Friendship;

import java.util.List;

public interface FriendshipMapper {
    int createFriendship(Friendship f);
    int deleteFriendship(int fid);
    Friendship queryFriendship(Friendship f);
    List<Friendship> queryFriends(int uid);
}
