package xchat.service.user.mappers;

import xchat.data.model.User;

public interface UserMapper {
    int insertUser(User user);
    User selectUserById(int uid);
    User selectUserByName(String name);
    int updatePassword(User user);
    int deleteUser(int uid);
}
