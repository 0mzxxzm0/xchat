package xchat.service.user;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@DubboComponentScan(basePackages = {"xchat.service.user.impl"})
public class UserServiceApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(UserServiceApplication.class, args);
        System.in.read();
    }
}
