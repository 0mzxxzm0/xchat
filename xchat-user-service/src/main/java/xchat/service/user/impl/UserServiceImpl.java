package xchat.service.user.impl;

import com.alibaba.dubbo.config.annotation.Service;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import xchat.data.model.Friendship;
import xchat.data.model.User;
import xchat.service.user.common.UserService;
import xchat.service.user.mappers.FriendshipMapper;
import xchat.service.user.mappers.UserMapper;

import java.util.ArrayList;
import java.util.List;

@Service(timeout = 3000, retries = -1)
public class UserServiceImpl implements UserService {
    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public int register(User user) {
        int uid = -1;
        SqlSession session = sqlSessionFactory.openSession(true);
        try {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            if (userMapper.insertUser(user) == 1) {
                uid = user.getUid();
            }
        } finally {
            session.close();
        }
        return uid;
    }

    @Override
    public int login(User user) {
        SqlSession session = sqlSessionFactory.openSession(true);
        int uid = -1;
        User realUser;
        try {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            realUser = userMapper.selectUserByName(user.getName());
        } finally {
            session.close();
        }
        if (realUser != null && realUser.getPassword().equals(user.getPassword())) {
            uid = realUser.getUid();
        }
        return uid;
    }

    @Override
    public User searchUserByName(String name) {
        SqlSession session = sqlSessionFactory.openSession(true);
        User user;
        try {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            user = userMapper.selectUserByName(name);
        } finally {
            session.close();
        }
        return user;
    }

    @Override
    public User searchUserById(int uid) {
        SqlSession session = sqlSessionFactory.openSession(true);
        User user;
        try {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            user = userMapper.selectUserById(uid);
        } finally {
            session.close();
        }
        return user;
    }

    // TODO : use redis to cache friends!
    @Override
    public boolean isFriends(Friendship f) {
        SqlSession session = sqlSessionFactory.openSession(true);
        boolean res;
        try {
            FriendshipMapper friendshipMapper = session.getMapper(FriendshipMapper.class);
            res = friendshipMapper.queryFriendship(f) != null;
        } finally {
            session.close();
        }
        return res;
    }

    @Override
    public List<User> getFriends(int uid) {
        SqlSession session = sqlSessionFactory.openSession(true);
        List<Friendship> friendships;
        try {
            FriendshipMapper friendshipMapper = session.getMapper(FriendshipMapper.class);
            friendships = friendshipMapper.queryFriends(uid);
        } finally {
            session.close();
        }
        List<User> users = new ArrayList<>();
        for (Friendship f : friendships) {
            if (f.getBigUid() == uid) {
                users.add(searchUserById(f.getSmallUid()));
            } else {
                users.add(searchUserById(f.getBigUid()));
            }
        }
        return users;
    }

    @Override
    public int createFriendship(Friendship friendship) {
        SqlSession session = sqlSessionFactory.openSession(true);
        int res;
        try {
            FriendshipMapper friendshipMapper = session.getMapper(FriendshipMapper.class);
            res = friendshipMapper.createFriendship(friendship);
        } finally {
            session.close();
        }
        return res;
    }
}
