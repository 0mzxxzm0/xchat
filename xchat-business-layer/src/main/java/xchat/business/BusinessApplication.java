package xchat.business;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@DubboComponentScan(basePackages = {"xchat.business.impl", "xchat.business"})
public class BusinessApplication {

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context =
                SpringApplication.run(BusinessApplication.class, args);
        System.in.read();
    }
}
