package xchat.business.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessResponse;
import xchat.business.common.BusinessResponseStatus;
import xchat.data.model.Friendship;
import xchat.data.model.Message;
import xchat.service.messagepush.common.MessagePushService;
import xchat.service.messagestore.common.MessageStoreService;
import xchat.service.user.common.UserService;
import java.util.List;

@Component
public class BusinessServiceMessage implements BusinessRequestHandler {
    private Logger log = LoggerFactory.getLogger(BusinessServiceMessage.class);
    @Reference
    private UserService userService;
    @Reference
    private MessageStoreService messageStoreService;
    @Reference
    private MessagePushService messagePushService;

    @Override
    public BusinessResponse handleRequest(BusinessRequest request) {
        BusinessResponse response =
                new BusinessResponse(BusinessResponseStatus.ERROR, "command not supported.", null);
        switch (request.getCommand()) {
            case "send":
                doSend(response, request);
                break;
            case "get_conversation":
                doGetConversation(response, request);
                break;
            case "get_unread_messages":
                doGetUnreadMessages(response, request);
                break;
        }
        return response;
    }

    public void doSend(BusinessResponse response, BusinessRequest request) {
        Message message = (Message) request.getParameter("message");
        if (message == null) {
            response.setInfo("invalid parameter.");
            return;
        }
        if (!userService.isFriends(new Friendship(message.getBigUid(), message.getSmallUid()))) {
            response.setInfo("you are trying to send a message to a stranger!");
            return;
        }
        log.info("invoke message store service to store message : {" + message.getContent() + "}");
        int mid = messageStoreService.storeMessage(message);
        if (mid == -1) {
            response.setInfo("fail to store message.");
            return;
        }
        message.setMid(mid);
        if (!messagePushService.pushMessage(message)) {
            response.setInfo("fail to push message.");
            return;
        }
        messageStoreService.readMessage(mid);
        response.setStatus(BusinessResponseStatus.OK);
        response.setInfo("");
    }

    public void doGetConversation(BusinessResponse response, BusinessRequest request) {
        Friendship f = new Friendship((int) request.getParameter("my_uid"), (int) request.getParameter("friend_uid"));
        long time = (long) request.getParameter("time");
        int limit = (int) request.getParameter("limit");
        response.setResult(messageStoreService.getConversation(f, time, limit));
        response.setStatus(BusinessResponseStatus.OK);
        response.setInfo("");
    }
    public void doGetUnreadMessages(BusinessResponse response, BusinessRequest request) {
        int uid = (int) request.getParameter("uid");
        List<Message> messageList = messageStoreService.getUnreadMessages(uid);
        for (Message message : messageList) {
            messageStoreService.readMessage(message.getMid());
        }
        response.setResult(messageList);
        response.setInfo("");
        response.setStatus(BusinessResponseStatus.OK);
    }
}
