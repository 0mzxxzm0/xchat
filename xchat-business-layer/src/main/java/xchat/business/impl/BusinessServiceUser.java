package xchat.business.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;
import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessResponse;
import xchat.business.common.BusinessResponseStatus;
import xchat.data.model.User;
import xchat.service.user.common.UserService;

@Component
public class BusinessServiceUser implements BusinessRequestHandler {
    @Reference
    private UserService userService;

    @Override
    public BusinessResponse handleRequest(BusinessRequest request) {
        BusinessResponse response =
                new BusinessResponse(BusinessResponseStatus.ERROR,"command not supported", null);
        switch (request.getCommand()) {
            case "register":
                doRegister(response, request);
                break;
            case "login":
                doLogin(response, request);
                break;
            case "search_by_name":
                doSearchUserByName(response, request);
                break;
            case "search_by_id":
                doSearchUserById(response, request);
                break;
        }
        return response;
    }

    void doRegister(BusinessResponse response, BusinessRequest request) {
        User user = (User) request.getParameter("userInfo");
        if (userService.searchUserByName(user.getName()) != null) {
            response.setInfo("user name already exists.");
            return;
        }
        int uid = userService.register(user);
        if (uid == -1) {
            response.setInfo("register failed!");
        } else {
            response.setStatus(BusinessResponseStatus.OK);
            response.setInfo("");
            response.setResult(uid);
        }
    }

    void doLogin(BusinessResponse response, BusinessRequest request) {
        User user = (User) request.getParameter("userInfo");
        int uid = userService.login(user);
        if (uid == -1) {
            response.setInfo("login failed!");
        } else {
            response.setStatus(BusinessResponseStatus.OK);
            response.setInfo("");
            response.setResult(uid);
        }
    }

    void doSearchUserByName(BusinessResponse response, BusinessRequest request) {
        String name = (String) request.getParameter("name");
        User user = userService.searchUserByName(name);
        if (user == null) {
            response.setInfo("no such user with named " + name + "!");
        } else {
            response.setStatus(BusinessResponseStatus.OK);
            response.setInfo("");
            response.setResult(user);
        }
    }

    void doSearchUserById(BusinessResponse response, BusinessRequest request) {
        int uid = (int) request.getParameter("uid");
        User user = userService.searchUserById(uid);
        if (user == null) {
            response.setInfo("no such user!");
        } else {
            response.setStatus(BusinessResponseStatus.OK);
            response.setInfo("");
            response.setResult(user);
        }
    }
}
