package xchat.business.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;
import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessResponse;
import xchat.business.common.BusinessResponseStatus;
import xchat.data.model.User;
import xchat.service.messagepush.common.MessagePushService;
import xchat.service.user.common.UserService;

@Component
public class BusinessServiceWebSocket implements BusinessRequestHandler {
    @Reference
    private UserService userService;
    @Reference
    private MessagePushService messagePushService;

    @Override
    public BusinessResponse handleRequest(BusinessRequest request) {
        BusinessResponse response =
                new BusinessResponse(BusinessResponseStatus.ERROR,"command not supported", null);
        switch (request.getCommand()) {
            case "open":
                doOpen(response, request);
                break;
            case "close":
                doClose(response, request);
                break;
        }
        return response;
    }

    public void doOpen(BusinessResponse response, BusinessRequest request) {
        User user = (User) request.getParameter("userInfo");
        int uid = userService.login(user);
        if (uid == -1) {
            response.setInfo("WebSocket login failed!");
            return;
        }
        String endpoint = (String) request.getParameter("endpoint");
        if (messagePushService.registerEndpoint(uid, endpoint)) {
            response.setStatus(BusinessResponseStatus.OK);
            response.setInfo("");
            user.setUid(uid);
            response.setResult(user);
        } else {
            response.setInfo("fail to register endpoint.");
        }
    }

    public void doClose(BusinessResponse response, BusinessRequest request) {
        int uid = (int) request.getParameter("uid");
        if (messagePushService.unRegisterEndpoint(uid)) {
            response.setStatus(BusinessResponseStatus.OK);
            response.setInfo("");
        } else {
            response.setInfo("fail to unregister endpoint.");
        }
    }
}
