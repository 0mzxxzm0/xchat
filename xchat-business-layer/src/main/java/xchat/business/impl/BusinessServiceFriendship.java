package xchat.business.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;
import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessResponse;
import xchat.business.common.BusinessResponseStatus;
import xchat.data.model.*;
import xchat.service.messagepush.common.MessagePushService;
import xchat.service.messagestore.common.MessageStoreService;
import xchat.service.user.common.UserService;

import java.util.List;

@Component
public class BusinessServiceFriendship implements BusinessRequestHandler {
    @Reference
    private MessageStoreService messageStoreService;
    @Reference
    private MessagePushService messagePushService;
    @Reference
    private UserService userService;

    @Override
    public BusinessResponse handleRequest(BusinessRequest request) {
        BusinessResponse response = new BusinessResponse(BusinessResponseStatus.ERROR, "command not supported", null);
        switch (request.getCommand()) {
            case "apply":
                doApply(response, request);
                break;
            case "approve":
                doApprove(response, request);
                break;
            case "get_friends":
                doGetFriends(response, request);
                break;
            case "get_friendship_applications":
                doGetFriendshipApplications(response, request);
                break;
        }
        return response;
    }

    public void doApply(BusinessResponse response, BusinessRequest request) {
        int from = (int)request.getParameter("from");
        int to = (int)request.getParameter("to");
        if (from == to) {
            response.setInfo("you cannot be a friend of yourself!");
            return;
        }
        User user = userService.searchUserById(to);
        if (user == null) {
            response.setInfo("user with uid(" + to + ") dose not exist.");
            return;
        }
        Friendship friendship = new Friendship(from, to);
        if (userService.isFriends(friendship)) {
            response.setInfo("you and " + user.getName() + " are already friends");
            return;
        }
        if (messageStoreService.getFriendshipApplication(from, to) != null) {
            response.setInfo("you have already send the application!");
            return;
        }
        Message friendshipApplication = new Message(from , to);
        friendshipApplication.setType(MessageType.APPLY4FRIENDSHIP);
        friendshipApplication.setStatus(MessageStatus.SENT);
        int mid = messageStoreService.storeMessage(friendshipApplication);
        if (mid == -1) {
            response.setInfo("fail to send the application!");
        } else {
            messagePushService.pushMessage(messageStoreService.getMessageById(mid));
            response.setStatus(BusinessResponseStatus.OK);
            response.setInfo("");
        }
    }

    public void doApprove(BusinessResponse response, BusinessRequest request) {
        int mid = (int)request.getParameter("mid");
        int uid = (int)request.getParameter("uid");
        Message applicationToApprove = messageStoreService.getMessageById(mid);
        if (applicationToApprove == null) {
            response.setInfo("no such application.");
            return;
        }
        int from, to;
        if (applicationToApprove.getDirection() == MessageDirection.BIG2SMALL) {
            from = applicationToApprove.getBigUid();
            to = applicationToApprove.getSmallUid();
        } else {
            from = applicationToApprove.getSmallUid();
            to = applicationToApprove.getBigUid();
        }
        if (to != uid || applicationToApprove.getType() != MessageType.APPLY4FRIENDSHIP) {
            response.setInfo("no such application!");
            return;
        }
        // TODO : delete message and create friendship should be in transaction. [DO THIS IN SERVICE LEVEL]
        if (messageStoreService.deleteMessage(mid) == 0) {
            response.setInfo("fail to approve the application.");
        } else {
            if (userService.createFriendship(new Friendship(from, to)) == 0) {
                response.setInfo("fail to create friendship.");
                return;
            }
            if (applicationToApprove.getDirection() == MessageDirection.BIG2SMALL) {
                applicationToApprove.setDirection(MessageDirection.SMALL2BIG);
            } else {
                applicationToApprove.setDirection(MessageDirection.BIG2SMALL);
            }
            applicationToApprove.setStatus(MessageStatus.READ);
            messagePushService.pushMessage(applicationToApprove);
            response.setStatus(BusinessResponseStatus.OK);
            response.setInfo("");
        }
    }

    public void doGetFriends(BusinessResponse response, BusinessRequest request) {
        int uid = (int)request.getParameter("uid");
        List<User> userList = userService.getFriends(uid);
        response.setInfo("");
        response.setStatus(BusinessResponseStatus.OK);
        response.setResult(userList);
    }

    public void doGetFriendshipApplications(BusinessResponse response, BusinessRequest request) {
        int uid = (int)request.getParameter("uid");
        response.setInfo("");
        response.setStatus(BusinessResponseStatus.OK);
        response.setResult(messageStoreService.getFriendshipApplications(uid));
    }
}
