package xchat.business.impl;

import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessResponse;

public interface BusinessRequestHandler {
    BusinessResponse handleRequest(BusinessRequest request);
}
