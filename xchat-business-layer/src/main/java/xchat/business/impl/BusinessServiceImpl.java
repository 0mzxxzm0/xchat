package xchat.business.impl;

import com.alibaba.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import xchat.business.common.BusinessRequest;
import xchat.business.common.BusinessResponse;
import xchat.business.common.BusinessResponseStatus;
import xchat.business.common.BusinessService;

@Service(timeout = 3000, retries = -1)
public class BusinessServiceImpl implements BusinessService {
    private static final Logger log = LoggerFactory.getLogger(BusinessService.class);
    @Autowired
    private BusinessServiceUser serviceUser;
    @Autowired
    private BusinessServiceFriendship serviceFriendship;
    @Autowired
    private BusinessServiceMessage serviceMessage;
    @Autowired
    private BusinessServiceWebSocket serviceWebSocket;

    @Override
    public BusinessResponse handleBusinessRequest(BusinessRequest request) {
        log.info(request.getType() + " " + request.getCommand());
        switch (request.getType()) {
            case USER:
                return serviceUser.handleRequest(request);
            case FRIENDSHIP:
                return serviceFriendship.handleRequest(request);
            case MESSAGE:
                return serviceMessage.handleRequest(request);
            case WEBSOCKET:
                return serviceWebSocket.handleRequest(request);
        }
        return new BusinessResponse(BusinessResponseStatus.ERROR, "", null);
    }
}