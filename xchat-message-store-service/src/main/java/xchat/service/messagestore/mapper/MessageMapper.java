package xchat.service.messagestore.mapper;

import xchat.data.model.Friendship;
import xchat.data.model.Message;
import org.apache.ibatis.annotations.Param;
import xchat.data.model.MessageStatus;

import java.util.List;
import java.util.Date;

public interface MessageMapper {
    int insertMessage(Message message);
    int deleteMessage(int mid);
    Message getMessageById(int mid);
    Message getFriendshipApplication(Message application);
    List<Message> getConversation(
            @Param("friends") Friendship friends,
            @Param("time") long time,
            @Param("size") int size);
    List<Message> getFriendshipApplications(int uid);
    int updateMessageStatus(
            @Param("mid") int mid,
            @Param("status") MessageStatus status);
    List<Message> getUnreadMessages(int uid);
}
