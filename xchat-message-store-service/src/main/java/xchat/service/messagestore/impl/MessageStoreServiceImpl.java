package xchat.service.messagestore.impl;

import com.alibaba.dubbo.config.annotation.Service;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import xchat.data.model.Friendship;
import xchat.data.model.Message;
import xchat.data.model.MessageStatus;
import xchat.data.model.MessageType;
import xchat.service.messagestore.common.MessageStoreService;
import xchat.service.messagestore.mapper.MessageMapper;

import java.util.Date;
import java.util.List;

@Service(timeout = 3000, retries = -1)
public class MessageStoreServiceImpl implements MessageStoreService {
    private Logger log = LoggerFactory.getLogger(MessageStoreServiceImpl.class);

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public int storeMessage(Message message) {
        log.info("store message : {" + message.getContent() + "}");
        int mid = -1;
        message.setTime(System.currentTimeMillis());
        SqlSession session = sqlSessionFactory.openSession(true);
        try {
            MessageMapper messageMapper = session.getMapper(MessageMapper.class);
            if (messageMapper.insertMessage(message) == 1)
                mid = message.getMid();
        } finally {
            session.close();
        }
        return mid;
    }

    @Override
    public int deleteMessage(int mid) {
        SqlSession session = sqlSessionFactory.openSession(true);
        int res;
        try {
            MessageMapper messageMapper = session.getMapper(MessageMapper.class);
            res = messageMapper.deleteMessage(mid);
        } finally {
            session.close();
        }
        return res;
    }

    @Override
    public Message getMessageById(int mid) {
        SqlSession session = sqlSessionFactory.openSession(true);
        Message message;
        try {
            MessageMapper messageMapper = session.getMapper(MessageMapper.class);
            message = messageMapper.getMessageById(mid);
        } finally {
            session.close();
        }
        return message;
    }

    @Override
    public List<Message> getConversation(Friendship f, long time, int count) {
        SqlSession session = sqlSessionFactory.openSession(true);
        List<Message> messageList;
        try {
            MessageMapper messageMapper = session.getMapper(MessageMapper.class);
            messageList = messageMapper.getConversation(f, time, count);
        } finally {
            session.close();
        }
        return messageList;
    }

    @Override
    public List<Message> getFriendshipApplications(int uid) {
        SqlSession session = sqlSessionFactory.openSession(true);
        List<Message> messageList;
        try {
            MessageMapper messageMapper = session.getMapper(MessageMapper.class);
            messageList = messageMapper.getFriendshipApplications(uid);
        } finally {
            session.close();
        }
        return messageList;
    }

    @Override
    public Message getFriendshipApplication(int from, int to) {
        Message application = new Message(from, to);
        application.setType(MessageType.APPLY4FRIENDSHIP);
        SqlSession session = sqlSessionFactory.openSession(true);
        Message message;
        try {
            MessageMapper messageMapper = session.getMapper(MessageMapper.class);
            message = messageMapper.getFriendshipApplication(application);
        } finally {
            session.close();
        }
        return message;
    }

    @Override
    public int readMessage(int mid) {
        SqlSession session = sqlSessionFactory.openSession(true);
        int res;
        try {
            MessageMapper messageMapper = session.getMapper(MessageMapper.class);
            res = messageMapper.updateMessageStatus(mid, MessageStatus.READ);
        } finally {
            session.close();
        }
        return res;
    }

    @Override
    public List<Message> getUnreadMessages(int uid) {
        List<Message> messageList;
        SqlSession session = sqlSessionFactory.openSession(true);
        try {
            MessageMapper messageMapper = session.getMapper(MessageMapper.class);
            messageList = messageMapper.getUnreadMessages(uid);
        } finally {
            session.close();
        }
        return messageList;
    }
}
